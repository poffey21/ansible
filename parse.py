#!/usr/local/bin/python

from base64 import b64decode
import os
import binascii
import sys


def main():
    user = os.environ['ANSIBLE_USER']
    password = os.environ.get('ANSIBLE_PASSWORD')
    key = os.environ.get('ANSIBLE_SSH_PRIVATE_KEY')
    masked = os.environ.get('ANSIBLE_SSH_PRIVATE_KEY_MASKED')
    key_file = 'id_rsa'
    
    if password:
        if key:
            sys.stderr.write('Using ANSIBLE_PASSWORD for authentication (ignoring ANSIBLE_SSH_PRIVATE_KEY)\n')
        if masked:
            sys.stderr.write('Using ANSIBLE_PASSWORD for authentication (ignoring ANSIBLE_SSH_PRIVATE_KEY_MASKED)\n')
        sys.stdout.write(f'ansible_user={user} ansible_password={password}')
        return
    
    if key:
        key += '\n'
        if masked:
            sys.stderr.write('Using ANSIBLE_SSH_PRIVATE_KEY for authentication (ignoring ANSIBLE_SSH_PRIVATE_KEY_MASKED)\n')
    elif masked:
        key = b64decode(masked).decode('UTF-8') + '\n'
    else:
        sys.stderr.write('Invalid attempt.')
        sys.exit(1)
    
    with open(key_file, 'w') as f:
        f.write(key)
    os.chmod(key_file, 600)  # This does not seem to take.
    sys.stdout.write(f'ansible_user={user} ansible_ssh_private_key_file=./{key_file}')
    

if __name__ == '__main__':
    main()
